import React from 'react';
import './Game.css';
// eslint-disable-next-line import/imports-first
import $ from "jquery";
import Board from './Board';



  export default class Game extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        history: [{
          squares: Array(400).fill(null)
        }],
        stepNumber: 0,
        xIsNext: true,
        isIncrease: true
      };
    }
  
  
    calculateWinner = squares => {

      let countCheo=1;
  
      for (let i = 0; i < 20; i+=1) {
      
          let countNgang = 1;
          let countDoc = 1;
          let checkTraiNgang = true;
          let checkPhaiNgang = true;
          let checkTraiDoc = true;
          let checkPhaiDoc = true;
          for(let j=0;j<19;j+=1) {
  
  
              if(squares[i*20+j]!=null && squares[i*20+j+1]!=null) {
                
                  if(squares[i*20+j] !== squares[i*20+j+1]) {
                      countNgang = 1;
                  } else {
                      countNgang+=1;
                      if(countNgang===5) {
                        for(let k=0;k<j;k+=1) {
                          if(squares[i*20+k]!=null) {
                            if(squares[i*20+k]!==squares[i*20+j+1]){
                              checkTraiNgang = false;
                            }
                          }
  
                        }
                        for(let k=18;k>=j+2;k-=1) {
                          if(squares[i*20+k]!=null) {
                            if(squares[i*20+k]!==squares[i*20+j+1]){
                              checkPhaiNgang = false;
                            }
                          }
                        }
                        if(checkTraiNgang===false&&checkPhaiNgang===false) {
                          countNgang = 1;
                        } else {
                          for(let count=j+1;count>j-4;count-=1) {
                            $(`#buttonStart${i*20+count}`).addClass('selectedButton');
                          }
                          return squares[i*20+j];
                        }
                      }
                  }
              } else {
                  countNgang = 1;
              }
  
              
              // Hàng dọc
              if (squares[j*20 + i] != null && squares[(j+1)* 20 + i] != null) {
                  if(squares[j*20 + i] !== squares[(j + 1)*20 + i]) {
                    countDoc=1;
                  }  else {
                      countDoc+=1;
  
                      if (countDoc === 5) {
                        
                        for(let k=0;k<j;k+=1) {
                          if(squares[k*20+i]!=null) {
                            if(squares[k*20+i]!==squares[(j+1)*20+i]) {
                              checkTraiDoc = false;
                            }
                          }
                        }
                        for(let k=18;k>=j+2;k-=1) {
                          if(squares[k*20+i]!=null) {
                            if(squares[k*20+i]!==squares[(j+1)*20+i]) {
                              checkPhaiDoc = false;
                            }
                          }
                        }
                        if(checkPhaiDoc===false&&checkTraiDoc===false) {
                          countDoc =1;
                        } else {
                          for(let count=j+1;count>j-4;count-=1) {
                            $(`#buttonStart${count*20+i}`).addClass('selectedButton');
                          }
                          return squares[j * 20 + i];
                        }
                      }
                  }
              } else {
                  countDoc=1;
              }
  
  
          }
      }
      // Đường chéo
      for (let i = 0; i < 16; i+=1) {
        for(let j=0;j<16;j+=1) {
          if (squares[i*20 + j] != null && squares[(i+1)* 20 + j+1] != null) {
            if(squares[i*20 + j] !== squares[(i+1)* 20 + j+1]) {
               countCheo =1;
            }  else {
  
              countCheo+=1;
                if (countCheo === 5) {
                  for(let k = i+1;k>=i-3;k-=1) {
                    for(let count=j+1;count>=j-3;count-=1) {
  
                      $(`#buttonStart${k*20+count}`).addClass('selectedButton');
                      k-=1;
                    }
                  }
                 countCheo = 1;  
                   return squares[i * 20 + j];
               }
               i+=1;
           }
       } else {
        countCheo =1;
       }
        }
      }
  
      for (let i = 19; i >=5; i-=1) {
        for(let j=19;j>=5;j-=1) {
          if (squares[i*20 + j] != null && squares[(i+1)* 20 + j+1] != null) {
            if(squares[i*20 + j] !== squares[(i+1)* 20 + j+1]) {
               countCheo =1;
            }  else {
  
              countCheo+=1;
                if (countCheo === 5) {
                  for(let k = i-1;k<=i+3;k+=1) {
                    for(let count=j-1;count<=j+3;count+=1) {
  
                      $(`#buttonStart${k*20+count}`).addClass('selectedButton');
                      k+=1;
                    }
                  }
                 countCheo = 1;  
                   return squares[i * 20 + j];
               }
               i-=1;
           }
       } else {
        countCheo =1;
       }
        }
      }
  
      let duongCheoPhu = 1;
      for (let i = 0; i <16; i+=1) {
        for(let j=19;j>=5;j-=1) {
          if (squares[i*20 + j] != null && squares[(i+1)* 20 + j-1] != null) {
            if(squares[i*20 + j] !== squares[(i+1)* 20 + j-1]) {
              duongCheoPhu =1;
            }  else {
              duongCheoPhu+=1;
                if (duongCheoPhu === 5) {
                  for(let k = i+1;k>=i-3;k-=1) {
                    for(let count=j-1;count<=j+3;count+=1) {
  
                      $(`#buttonStart${k*20+count}`).addClass('selectedButton');
                      k-=1;
                    }
                  }
                  duongCheoPhu = 1;
                   return squares[i * 20 + j];
               }
               i+=1;
           }
       } else {
        duongCheoPhu =1;
       }
        }
      }
  
  
      for (let i = 0; i <16; i+=1) {
        for(let j=19;j>0;j-=1) {
          if (squares[i*20 + j] != null && squares[(i+1)* 20 + j-1] != null) {
            if(squares[i*20 + j] !== squares[(i+1)* 20 + j-1]) {
              duongCheoPhu =1;
            }  else {
              duongCheoPhu+=1;
                if (duongCheoPhu === 5) {
                  for(let k = i-1;k<=i+3;k+=1) {
                    for(let count=j+1;count>=j-3;count-=1) {
                      $(`#buttonStart${k*20+count}`).addClass('selectedButton');
                      k+=1;
                    }
                  }
                  duongCheoPhu = 1;
                   return squares[i * 20 + j];
               }
               i+=1;
           }
       } else {
        duongCheoPhu =1;
       }
        }
      }
  
      return null;
    }

    handleClick(i) {
      const {stepNumber} = this.state;
      const {history} = this.state;
      const historyy = history.slice(0, stepNumber + 1);
      const current = history[historyy.length - 1];
      const squares = current.squares.slice();
      if (this.calculateWinner(squares) || squares[i]) {
        return;
      }
      const {xIsNext} = this.state;
      squares[i] = xIsNext ? 'X' : 'O';
      this.setState({
        history: history.concat([{
          squares
        }]),
        stepNumber: history.length,
        xIsNext: !xIsNext,
      });
    }
    
    jumpTo(step) {
      const {history} = this.state;
      for(let i=0;i<400;i+=1) {
        $(`#buttonStart${i}`).removeClass('selectedButton');  
      }
      for(let i=0;i<history.length;i+=1) {
        $(`#button${i}`).removeClass('selectedButton');    
      }
      $(`#button${step}`).addClass('selectedButton');     
      this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0,
      });
    }

    render() {

      const {history, stepNumber, isIncrease,xIsNext} = this.state;
      const current = history[stepNumber];
      const winner = this.calculateWinner(current.squares);

      const moves =[];

      if(isIncrease===true) {
        for(let i=0;i<history.length;i+=1) {
          const desc = i ?
          `Đi tới bước thứ ${  i}` :
          'Bắt đầu lại từ đầu';
          moves.push(          <li key={i}>
            <button type="button" id={`button${i}`} onClick={() => this.jumpTo(i)}>{desc}</button>
            </li> );
        }
      } else {
        for(let i=history.length-1;i>=0;i-=1) {
          const desc = i ?
          `Đi tới bước thứ ${  i}` :
          'Bắt đầu lại từ đầu';
          moves.push(          <li key={i}>
            <button type="button" id={`button${i}`} onClick={() => this.jumpTo(i)}>{desc}</button>
            </li> );
        }
      }

      let status;
      if (winner) {
        status = `Xin chúc mừng, người chiến thắng là: ${  winner}`;
      } else {
        status = `Lượt chơi tiếp theo: ${  xIsNext ? 'X' : 'O'}`;
      }
  
      return (
        
        <div className="game">
          <div className="game-board">
            <Board
              squares={current.squares}
              onClick={(i) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div> 
            <div><button type="button" onClick={()=>{
                  
                    this.setState({
                      isIncrease: !isIncrease
                    });
                  
            }}>Sắp xếp các bước đi</button></div>
            <div style={{overflow: 'scroll', height: '80vh'} }>
            <ul>{moves}</ul>
            </div>
          </div>
        </div>
      );
    }

  }
  
  // ========================================
  
 
    